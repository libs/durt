const String getHistoryQuery = r'''
  query ($pubkey: String!, $number: Int!, $cursor: String) {
      txsHistoryBc(
          script: $pubkey
          pagination: { pageSize: $number, ord: DESC, cursor: $cursor }
    ) {
      both {
          pageInfo {
              hasPreviousPage
              hasNextPage
              startCursor
              endCursor
          }
          edges {
              direction
              node {
                  currency
                  issuers
                  outputs
                  comment
                  writtenTime
              }
          }
      }
    }
    txsHistoryMp(pubkey: $pubkey) {
        receiving {
            currency
            issuers
            comment
            outputs
        }
        sending {
            currency
            issuers
            comment
            outputs
        }
    }
    currentUd {
      amount
      base
    }
    balance(script: $pubkey) {
      amount
      base
  }
}
''';

const String getBalanceQuery = r'''
query ($pubkey: String!) {
  balance(script: $pubkey) {
    amount
    base
  }
  currentUd {
    amount
    base
  }
}
''';

const String getWalletsQuery = r'''
query ($number: Int!, $cursor: String) {
  wallets(pagination: {ord: ASC, pageSize: $number, cursor: $cursor}) {
    pageInfo {
      hasNextPage
      endCursor
    }
    edges {
      node {
        script
        balance {
          amount
          base
        }
        idty {
          isMember
          username
        }
      }
    }
  }
}
''';

const String getIdQuery = r'''
query ($pubkey: PubKeyGva!) {
  idty(pubkey: $pubkey) {
    isMember
    username
  }
}
''';

const String genTransactionDocQuery = r'''
query ($recipient: PkOrScriptGva!, $issuer: PubKeyGva!, $amount: Int!, $comment: String!, $useMempool: Boolean!) {
  genTx(
    amount: $amount
    comment: $comment
    issuer: $issuer
    recipient: $recipient
    useMempoolSources: $useMempool
  )
}
''';

const String genComplexTransactionDocQuery = r'''
query ($issuers: [TxIssuer!]!, $recipients: [TxRecipient!]!, $comment: String!, $useMempool: Boolean!) {
  genComplexTx(
    issuers: $issuers
    recipients: $recipients
    comment: $comment
    useMempoolSources: $useMempool
  ) {
    changes
    tx
  }
}
''';

const String sendTransactionDocQuery = r'''
mutation ($signedDoc: String!){
  tx(
    rawTx: $signedDoc
    ) {
        version
        issuers
        outputs
    }
}
''';

const String currentUdQuery = r'''
query {
  currentUd {
    amount
    base
  }
}
''';

const String currentBlockQuery = r'''
query {
  currentBlock {
    number,
  }
}
''';

const String currentBlockExtendedQuery = r'''
query {
  currentBlock {
    version
    number
    hash
    signature
    innerHash
    previousHash
    issuer
    time
    powMin
    membersCount
    issuersCount
    issuersFrame
    medianTime
    nonce
    monetaryMass
    unitBase
    dividend
  }
}
''';

const String getFirstUtxosQuery = r'''
query GetFirstUtxos($scripts: [PkOrScriptGva!]!, $first: Int!) {
  firstUtxosOfScripts(scripts: $scripts, first: $first) {
    amount
    base
    txHash
    outputIndex
  }
}
''';

const String getUtxosOfScriptQuery = r'''
query GetUtxosOfScript($script: PkOrScriptGva!, $pageSize: Int!, $amount: Int, $cursor: String) {
  utxosOfScript(
    script: $script,
    pagination: { pageSize: $pageSize, ord: ASC, cursor: $cursor },
    amount: $amount
  ) {
    pageInfo {
      hasNextPage
    }
    edges {
      node {
        amount
        base
        txHash
        outputIndex
        writtenTime
        writtenBlock
      }
    }
    aggregate {
      sum
    }
  }
}
''';
