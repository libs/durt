import 'dart:convert';
import 'package:durt/src/crypto/dewif.dart';
import 'package:fast_base58/fast_base58.dart';
import 'package:bip32_ed25519/api.dart';
import 'package:bip39_multi_nullsafety/bip39_multi_nullsafety.dart' as bip39;
// import 'package:hex/hex.dart';
import 'package:pinenacl/key_derivation.dart';
import 'package:pointycastle/digests/sha256.dart';
import 'package:pointycastle/digests/sha512.dart';
import 'package:pointycastle/pointycastle.dart'
    show KeyDerivator, ScryptParameters;

class HdWallet {
  Bip32KeyPair key;

  /// Root constructor taking a root signing key
  HdWallet({required this.key});

  /// Build the root signing key based on given mnemonic.
  factory HdWallet.fromMnemonic(String mnemonic, {bool isScrypt = true}) {
    mnemonic = mnemonic.replaceAll('é', 'é');
    mnemonic = mnemonic.replaceAll('è', 'è');

    Bip32SigningKey rootSigningKeyFactory =
        _bip32signingKey(mnemonic, isScrypt: isScrypt);
    return HdWallet(
        key: Bip32KeyPair(
            signingKey: rootSigningKeyFactory,
            verifyKey: rootSigningKeyFactory.verifyKey));
  }

  factory HdWallet.fromDewif(String dewif, String password,
      {String lang = 'english',
      int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION,
      bool isScrypt = true}) {
    final mnemonic = Dewif().mnemonicFromDewif(dewif, password,
        lang: lang,
        dewifCurrencyCode: dewifCurrencyCode,
        dewifVersion: dewifVersion);
    return HdWallet.fromMnemonic(mnemonic, isScrypt: isScrypt);
  }

  /// return the root signing key
  Bip32VerifyKey? get rootVerifyKey => key.verifyKey;

  /// derive root signing key given a seed
  static Bip32SigningKey _bip32signingKey(String mnemonic,
      {bool isScrypt = true}) {
    Uint8List sha256(List<int> data) =>
        SHA256Digest().process(Uint8List.fromList(data));
    final mnemonicUtf8 = Uint8List.fromList(utf8.encode(mnemonic));
    // final mnemonicBytes = Uint8List.fromList(mnemonic.codeUnits);
    final salt = sha256('dubp'.codeUnits + mnemonicUtf8);

    final Uint8List seed;

    if (isScrypt) {
      // Scrypt seed version
      final scrypt = KeyDerivator('scrypt')
        ..init(
          ScryptParameters(
            4096, //2^12
            16,
            1,
            32,
            salt,
          ),
        );
      seed = scrypt.process(mnemonicUtf8);
    } else {
      // Pbkdf2 seed version
      seed = PBKDF2.hmac_sha512(mnemonicUtf8, salt, 4096, 32);
      // With pointyCastle lib: Very slower
      // final rawMaster = KeyDerivator('SHA-512/HMAC/PBKDF2')
      //   ..init(Pbkdf2Parameters(mnemonicUtf8, 4096, 32));
    }

    final privateKey = SHA512Digest().process(seed);
    final chainCode = sha256([1] + seed);

    return Bip32SigningKey.normalizeBytes(
        Uint8List.fromList(privateKey + chainCode));
  }

  /// If a parent signing key is provided, a child signing key is generated. If a parent
  /// verify key is provided and the index is NOT hardened, then a child verify key is
  /// also included. If hardened and no signingKey is provied, it returns an empty pair.
  Bip32KeyPair derive(int index) {
    // computes a child extended private key from the parent extended private key.
    final derivator = Bip32Ed25519KeyDerivation.instance;

    Bip32SigningKey? signingKey = key.signingKey != null
        ? derivator.ckdPriv(key.signingKey!, harden(index)) as Bip32SigningKey
        : null;
    return Bip32KeyPair(
        signingKey: signingKey, verifyKey: signingKey?.verifyKey);
  }

  /// Provide a valid signature from given derivation keypair for any message or text document.
  String sign(String document, {required int derivation}) {
    Bip32KeyPair keypair = derive(derivation);
    return base64Encode(
        keypair.signingKey!.sign(document.codeUnits).signature.asTypedList);
  }

  /// Verify if the given signature is valid for the given message or text document,
  /// based on given keypair
  bool verifySign(String document, String signature,
      {required int derivation}) {
    Bip32KeyPair keypair = derive(derivation);
    SignatureBase fomartedSignature = Signature(base64Decode(signature));
    return keypair.verifyKey!.verify(
        signature: fomartedSignature,
        message: document.codeUnits.toUint8List());
  }

  /// Return the pubkey in string format for the given derivation keypair
  String getPubkey(int derivation) {
    Bip32KeyPair subKey = derive(derivation);
    return Base58Encode(subKey.signingKey!.publicKey.rawKey.toList());
  }
}

/// Private/signing and public/varification key pair.
class Bip32KeyPair {
  final Bip32SigningKey? signingKey;
  final Bip32VerifyKey? verifyKey;
  const Bip32KeyPair({this.signingKey, this.verifyKey});
}

/// Return a 12 random worlds mnemonic based on bip39 standard.
/// You can choose a custom language like french, default is english.
String generateMnemonic({String lang = 'english'}) {
  return bip39.generateMnemonic(strength: 128, language: lang);
}

/// Hardended chain values should not have public keys.
/// They are denoted by a single quote in chain values.
const int hardenedOffset = 0x80000000;

/// Hardens index, meaning it won't have a public key
int harden(int index) => index | hardenedOffset;

/// Returns true if index is hardened.
bool isHardened(int index) => index & hardenedOffset != 0;
