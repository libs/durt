import 'package:durt/src/crypto/cesium_wallet.dart';
import 'package:test/test.dart';

void main() {
  group('cesium', () {
    test('Cesium wallet creation and signature', () {
      var cesiumWallet = CesiumWallet('toto', 'tata');

      var message = 'tata cesium testy';

      var signature = cesiumWallet.sign(message);
      bool isOK = cesiumWallet.verifySign(message, signature);

      expect(
          cesiumWallet.pubkey, 'A6o88pwgb24Ri3N6UEGALLGhVAm7vrK8fJYHxmw91JSc');
      expect(isOK, true);
    });
  });
}
