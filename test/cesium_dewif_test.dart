import 'dart:typed_data';
import 'package:durt/src/crypto/cesium_wallet.dart';
import 'package:durt/src/crypto/dewif.dart';
import 'package:test/test.dart';

void main() {
  group('cesium', () {
    test('Cesium wallet DEWIF encryption and decryption', () async {
      print('------------------\n Cesium DEWIF test\n------------------\n');
      final cesiumWallet = CesiumWallet('toto', 'tata');
      Dewif dewif = Dewif();

      final dewifDataTest = await dewif.generateCesiumDewif(
          cesiumWallet.seed, 'JKSPU',
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST, test: true);
      print('Lock: ${dewifDataTest.dewif}');

      expect(dewifDataTest.dewif,
          'AAAAARAAAAEOAcVCma5x/ipOzcfViv8J5cdgASn7zedlNz6M/19wbE79aNvILr1CEyM664m8BjgLKeWiz1GYJSR14+ZY61U=');

      Uint8List? decryptedDewifTest;
      try {
        decryptedDewifTest = dewif.cesiumSeedFromDewif(
            dewifDataTest.dewif, 'JKSPU',
            dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);
        print('Unlock: $decryptedDewifTest');
      } on ChecksumException {
        print('Bad secret code');
      } catch (e) {
        print(e);
      }
      expect(
          decryptedDewifTest,
          equals([
            218,
            9,
            63,
            33,
            79,
            188,
            150,
            119,
            73,
            112,
            60,
            104,
            121,
            153,
            74,
            219,
            187,
            169,
            221,
            222,
            100,
            88,
            195,
            12,
            84,
            214,
            238,
            116,
            63,
            37,
            238,
            124
          ]));

      final reCesiumWallet = CesiumWallet.fromSeed(decryptedDewifTest!);
      // [218, 9, 63, 33, 79, 188, 150, 119, 73, 112, 60, 104, 121, 153, 74, 219, 187, 169, 221, 222, 100, 88, 195, 12, 84, 214, 238, 116, 63, 37, 238, 124]

      expect(reCesiumWallet.pubkey,
          'A6o88pwgb24Ri3N6UEGALLGhVAm7vrK8fJYHxmw91JSc');

      final rereCesiumWallet = CesiumWallet.fromDewif(
          dewifDataTest.dewif, dewifDataTest.password,
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST, test: true);

      expect(rereCesiumWallet.pubkey,
          'A6o88pwgb24Ri3N6UEGALLGhVAm7vrK8fJYHxmw91JSc');

      // Change Dewif password
      final changeDewifPassword = await dewif.changeCesiumPassword(
          dewif: dewifDataTest.dewif,
          oldPassword: dewifDataTest.password,
          newPassword: 'NBVCP',
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);

      expect(changeDewifPassword.password, 'NBVCP');

      final unlockChangeDewifPassword = dewif.cesiumSeedFromDewif(
          changeDewifPassword.dewif, 'NBVCP',
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);
      expect(
          unlockChangeDewifPassword,
          equals([
            218,
            9,
            63,
            33,
            79,
            188,
            150,
            119,
            73,
            112,
            60,
            104,
            121,
            153,
            74,
            219,
            187,
            169,
            221,
            222,
            100,
            88,
            195,
            12,
            84,
            214,
            238,
            116,
            63,
            37,
            238,
            124
          ]));
    });
  });
}
